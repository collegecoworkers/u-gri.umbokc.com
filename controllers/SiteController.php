<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

use app\models\Order;
use app\models\Event;
use app\models\Category;

class SiteController extends Controller {

	public function behaviors() {
		return [
		];
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function beforeAction($action) {

		// if (Yii::$app->user->isGuest) {
		// 	return $this->redirect('/auth/login');
		// }

		return parent::beforeAction($action);
	}

	public function actionIndex() {
		$new_events = Event::getNew(3);
		$events = Event::getNew(6);

		return $this->render('index',[
			'new_events'=>$new_events,
			'events'=>$events,
		]);
	}

	public function actionCat($cat) {
		$cat = Category::findOne($cat);
		$events = Event::getAll($cat);

		return $this->render('cat',[
			'cat'=>$cat,
			'events'=>$events,
		]);
	}

	public function actionMyEvents() {
		$orders = Order::find()->where(['user_id' => Yii::$app->user->id])->all();
		$order_ids = [];

		foreach ($orders as $item) {
			$order_ids[] = $item->event_id;
		}
		$events = Event::find()->where(['id' => $order_ids])->all();

		return $this->render('my',[
			'events'=>$events,
		]);
	}

	public function actionView($id) {
		$event = Event::findOne($id);
		$order = new Order();

		if(Yii::$app->request->isPost) {
			if(Yii::$app->user->isGuest)
				return $this->redirect('/auth/login');

			$order->load(Yii::$app->request->post());
			$order->user_id = Yii::$app->user->id;
			$order->save();
			$event->count = (int) $event->count;
			$event->count -= 1;
			$event->save();
			return $this->redirect('/site/my-events');
		}

		return $this->render('single',[
			'event'=>$event,
			'order'=>$order,
		]);
	}

}
