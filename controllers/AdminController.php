<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;

use app\models\Order;
use app\models\Category;
use app\models\Event;
use app\models\User;
use app\models\ImageUpload;


class AdminController extends Controller {

	public $layout = 'admin';

	public function behaviors() {
		return [
		];
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function beforeAction($action) {

		if (Yii::$app->user->isGuest) {
			return $this->redirect('/auth/login');
		}

		return parent::beforeAction($action);
	}

	public function actionIndex() {
		$orders = Order::find()->orderBy(['id' => SORT_DESC])->limit(10)->all();

		return $this->render('index',[
			'orders'=>$orders,
		]);
	}

	public function actionOrders() {
		$orders = Order::find()->orderBy(['id' => SORT_DESC])->all();

		return $this->render('orders',[
			'orders'=>$orders,
		]);
	}

	public function actionUsers() {

		$this->check_admin();

		$users = User::find()->all();

		return $this->render('user-list',[
			'users'=>$users,
		]);
	}


	public function actionCats() {

		$this->check_admin();

		$categories = Category::find()->orderBy(['id' => SORT_DESC])->all();

		return $this->render('cats',[
			'categories'=>$categories,
		]);
	}

	public function actionCatAdd() {

		$this->check_admin();

		$model = new Category();

		if($model->load(Yii::$app->request->post()) and $model->save()){
			return $this->redirect('/admin/cats');
		}

		return $this->render('cat-add',[
			'model'=>$model,
		]);
	}

	public function actionCatEdit($id) {

		$this->check_admin();

		$model = Category::findOne($id);

		if($model->load(Yii::$app->request->post()) and $model->save()){
			return $this->redirect('/admin/cats');
		}

		return $this->render('cat-edit',[
			'model'=>$model,
		]);
	}

	public function actionCatDelete($id) {

		$this->check_admin();

		$model = Category::findOne($id);
		$model->delete();

		return $this->redirect(['/admin/cats']);
	}


	public function actionEvent() {
		$this->check_admin();

		$events = Event::find()->orderBy(['id' => SORT_DESC])->all();

		return $this->render('events',[
			'events'=>$events,
		]);
	}

	public function actionEventAdd() {

		$this->check_admin();

		$model = new Event();
		$model_iu = new ImageUpload;

		if($model->load(Yii::$app->request->post())){

			$file = UploadedFile::getInstance($model,'image');
			if($file != null)
				$model->image = $model_iu->uploadFile($file, $model->image);

			if($model->save()){
				return $this->redirect('/admin/event');
			}

		}

		return $this->render('event-add',[
			'model_iu'=>$model_iu,
			'model'=>$model,
		]);
	}

	public function actionEventEdit($id) {

		$this->check_admin();

		$model = Event::findOne($id);
		$model_iu = new ImageUpload;

		if($model->load(Yii::$app->request->post())){

			$file = UploadedFile::getInstance($model,'image');
			if($file != null)
				$model->image = $model_iu->uploadFile($file, $model->image);

			if($model->save()){
				return $this->redirect('/admin/event');
			}

		}

		return $this->render('event-edit',[
			'model'=>$model,
		]);
	}

	public function actionEventDelete($id) {

		$this->check_admin();

		$model = Event::findOne($id);
		$model->delete();

		return $this->redirect(['/admin/event']);
	}


	public function check_admin(){
		if(!User::is_admin())
			return $this->redirect('/admin/index');
	}

}
