<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PublicAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	
	public $css = [
		'https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,300',
		'/public/css/animate.css',
		'/public/css/icomoon.css',
		'/public/css/bootstrap.css',
		'/public/css/superfish.css',
		'/public/css/flexslider.css',
		'/public/css/magnific-popup.css',
		'/public/css/bootstrap-datepicker.min.css',
		'/public/css/cs-select.css',
		'/public/css/cs-skin-border.css',
		'/public/css/style.css',
		"css/site.css",
	];
	
	public $js = [
		'/public/js/modernizr-2.6.2.min.js',
		'/public/js/respond.min.js',
		// '/public/js/jquery.min.js',
		'/public/js/jquery.easing.1.3.js',
		'/public/js/bootstrap.min.js',
		'/public/js/jquery.waypoints.min.js',
		'/public/js/sticky.js',
		'/public/js/hoverIntent.js',
		'/public/js/superfish.js',
		'/public/js/jquery.flexslider-min.js',
		'/public/js/bootstrap-datepicker.min.js',
		'/public/js/classie.js',
		'/public/js/selectFx.js',
		'/public/js/main.js',
	];

	public $depends = [
		'yii\web\YiiAsset',
		// 'yii\bootstrap\BootstrapAsset',
	];
}
