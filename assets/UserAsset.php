<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class UserAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	
	public $css = [
		'http://fonts.googleapis.com/css?family=Open+Sans:400,700',
		'/admin/lib/bootstrap/css/bootstrap.css',
		'/admin/lib/font-awesome/css/font-awesome.css',
		'/admin/stylesheets/theme.css',
		'/admin/stylesheets/premium.css',
		'/admin/stylesheets/main.css',
		// "public/style.css",
	];
	
	public $js = [
		// '/admin/lib/jquery-1.11.1.min.js',
		'/admin/lib/jQuery-Knob/js/jquery.knob.js',
		'/admin/lib/bootstrap/js/bootstrap.js',
		'/admin/js/main.js',
	];

	public $depends = [
		'yii\web\YiiAsset',
		// 'yii\bootstrap\BootstrapAsset',
	];
}
