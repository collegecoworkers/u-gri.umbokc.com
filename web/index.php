<?php
// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

// Author: umbokc
// function for debug
function dbg($s, $d = true){
  echo "<pre>";
  if($s)
    print_r($s);
  else
    var_dump($s);
  echo "\n</pre>";
  if($d)
    die;
}

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../config/web.php');

(new yii\web\Application($config))->run();