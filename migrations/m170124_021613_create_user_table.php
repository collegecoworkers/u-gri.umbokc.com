<?php

use yii\db\Migration;

class m170124_021613_create_user_table extends Migration
{
	public function up()
	{
		$this->createTable('user', [
			'id' => $this->primaryKey(),
			'name'=>$this->string(),
			'email'=>$this->string()->defaultValue(null),
			'password'=>$this->string(),
			'is_admin'=>$this->integer()->defaultValue(0),
		]);
	}

	public function down()
	{
		$this->dropTable('user');
	}
}
