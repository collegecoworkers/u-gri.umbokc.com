<?php

use yii\db\Migration;

class m170124_021608_create_order_table extends Migration {

	public function up() {
		$this->createTable('order', [
			'id' => $this->primaryKey(),
			'user_id'=>$this->integer(),
			'event_id'=>$this->integer(),
			'card_num'=>$this->string(),
			'card_cvv'=>$this->string(),
			'card_date'=>$this->string(),
			'date'=>$this->date(),
		]);
	}

	public function down() {
		$this->dropTable('order');
	}
}
