<?php

namespace app\models;

use Yii;

class Order extends \yii\db\ActiveRecord
{
	public static function tableName()
	{
		return 'order';
	}

	public function rules()
	{
		return [
			[['card_cvv','card_num','card_date','event_id'], 'required'],
			[['date'],'default', 'value' => date('Y-m-d')],
			// [['card_cvv'], 'number', 'max' => 999, 'min' => 100],
			// [['card_num'], 'match', 'pattern' => '~[0-9]{4} [0-9]{4} [0-9]{4} [0-9]{4}$~'],
			// [['card_date'], 'match', 'pattern' => '~[0-9]{2}/[0-9]{2}$~'],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'user_id' => 'Покупатель',
			'event_id' => 'Продукт',
			'date' => 'Дата',
			'card_num' => 'Номер карты',
			'card_cvv' => 'CVV карты',
			'card_date' => 'Дата карты',
		];
	}

	public function getByEvent($event_id)
	{
		return self::find()->where(['event_id' => $event_id])->all();
	}

	public function getEvent(){
		return Event::find()->where(['id' => $this->event_id])->one();
	}

}
