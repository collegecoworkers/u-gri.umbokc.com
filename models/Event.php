<?php

namespace app\models;

use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;

class Event extends \yii\db\ActiveRecord
{

	public static function tableName()
	{
		return 'event';
	}

	public function rules()
	{
		return [
			[['title'], 'required'],
			[['title','desc','content'], 'string'],
			[['title'], 'string', 'max' => 255],
			[['category_id', 'price', 'count'], 'number'],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'title' => 'Заголовок',
			'desc' => 'Описание',
			'content' => 'Контент',
			'price' => 'Цена',
			'image' => 'Изображение',
			'count' => 'Кол-во',
			'category_id' => 'Категория',
		];
	}

	public function saveEvent()
	{
		return $this->save();
	}

	public function saveImage($filename)
	{
		$this->image = $filename;
		return $this->save(false);
	}

	public function getImage()
	{
		return ($this->image) ? '/uploads/' . $this->image : 'https://placehold.it/200x300';
	}

	public function getBigImage()
	{
		return ($this->image) ? '/uploads/' . $this->image : 'https://placehold.it/983x600';
	}

	public function deleteImage()
	{
		$imageUploadModel = new ImageUpload();
		$imageUploadModel->deleteCurrentImage($this->image);
	}

	public function beforeDelete()
	{
		$this->deleteImage();
		return parent::beforeDelete(); // TODO: Change the autogenerated stub
	}

	public function getCategory()
	{
		return Category::find()->where(['id' => $this->category_id])->one();
	}

	public function getOrders()
	{
		return Order::find()->where(['event_id' => $this->id])->all();
	}

	public static function getAll($cat = null)
	{
		if($cat === null)
			return self::find()->all();
		else
			return self::find()->where(['category_id' => $cat])->all();
	}

	public static function getNew($lim)
	{
		return self::find()->orderBy('id DESC')->limit($lim)->all();
	}

}
