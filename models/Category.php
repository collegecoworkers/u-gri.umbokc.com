<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

class Category extends \yii\db\ActiveRecord
{
	public static function tableName()
	{
		return 'category';
	}

	public function rules()
	{
		return [
			[['title'], 'string', 'max' => 255],
			[['desc'], 'string'],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'title' => 'Название',
			'desc' => 'Описание',
		];
	}

	public static function getAllArr()
	{
		return ArrayHelper::map(self::getAll(), 'id', 'title');
	}

	public static function getAll()
	{
		return self::find()->all();
	}
}
