<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Добавление категории';
?>
<?= $this->render('_form-cat', [
	'model' => $model,
]) ?>
