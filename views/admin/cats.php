<?php 
use app\models\Event;
use app\models\User;

$this->title = 'Категории';
?>
<div class="header">
	<h1 class="page-title"><?= $this->title ?></h1>
	<ul class="breadcrumb">
		<li><a href="/">Сайт</a> </li>
		<li><a href="/admin/index">Админ панель</a> </li>
		<li><?= $this->title ?></li>
	</ul>
</div>
<div class="main-content">
	<div class="btn-toolbar list-toolbar">
		<a href="/admin/cat-add" class="btn btn-primary"><i class="fa fa-plus"></i> Добавить</a>
	</div>
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<table class="table">
				<thead>
					<tr>
						<th>Название</th>
						<th>Кол-во продуктов</th>
						<th>Действия</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($categories as $item): ?>
						<?php
							$events = Event::find()->where(['category_id' => $item->id])->count();
						?>
						<tr>
							<td><?= $item->title ?></td>
							<td><?= $events ?></td>
							<td>
								<a href="/admin/cat-edit/?id=<?= $item->id ?>"><i class="glyphicon glyphicon-pencil"></i></a>
								<a href="/admin/cat-delete/?id=<?= $item->id ?>" onclick="return confirm('Вы уверенны?')"><i class="glyphicon glyphicon-trash"></i></a>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
