<?php 
use app\models\Category;
use app\models\Order;

$this->title = 'Билеты';
?>
<div class="header">
	<h1 class="page-title"><?= $this->title ?></h1>
	<ul class="breadcrumb">
		<li><a href="/">Сайт</a> </li>
		<li><a href="/admin/index">Админ панель</a> </li>
		<li><?= $this->title ?></li>
	</ul>
</div>
<div class="main-content">
	<div class="btn-toolbar list-toolbar">
		<a href="/admin/event-add" class="btn btn-primary"><i class="fa fa-plus"></i> Добавить</a>
	</div>
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<table class="table">
				<thead>
					<tr>
						<th>Название</th>
						<th>Категория</th>
						<th>Описание</th>
						<th>Цена</th>
						<th>Кол-во покупок</th>
						<th>Действия</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($events as $item): ?>
						<?php
							$cat = Category::find()->where(['id' => $item->category_id])->one()->title;
							$orders = Order::find()->where(['event_id' => $item->id])->count();
						?>
						<tr>
							<td><?= $item->title ?></td>
							<td><?= $cat ?></td>
							<td style="max-width: 150px" class="truncate"><?= $item->desc ?></td>
							<td>$<?= $item->price ?></td>
							<td><?= $orders ?></td>
							<td>
								<a href="/admin/event-edit/?id=<?= $item->id ?>"><i class="glyphicon glyphicon-pencil"></i></a>
								<a href="/admin/event-delete/?id=<?= $item->id ?>" onclick="return confirm('Вы уверенны?')"><i class="glyphicon glyphicon-trash"></i></a>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
