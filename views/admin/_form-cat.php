<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="header">
	<h1 class="page-title"><?= $this->title ?></h1>
	<ul class="breadcrumb">
		<li><a href="/">Сайт</a> </li>
		<li><a href="/admin/index">Админ панель</a> </li>
		<li><?= $this->title ?></li>
	</ul>
</div>
<div class="main-content">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading no-collapse">Заполните форму</div>
				<div class="" style="margin: 1em;">
					<?php $form = ActiveForm::begin(); ?>

					<?= $form->field($model, 'title')->textInput() ?>
					<?= $form->field($model, 'desc')->textArea() ?>

					<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success' ]) ?>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>