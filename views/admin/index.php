<?php 
use app\models\Event;
use app\models\User;

$this->title = 'Админ панель';
?>
<div class="header">
	<h1 class="page-title"><?= $this->title ?></h1>
	<ul class="breadcrumb">
		<li><a href="/">Сайт</a> </li>
		<li><?= $this->title ?></li>
	</ul>
</div>
<div class="main-content">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading no-collapse">Последние заказы</div>
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Билет</th>
							<th>Пользователь</th>
							<th>Дата</th>
							<th>Цена</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($orders as $item): ?>
						<?php $event = Event::findOne($item->event_id); ?>
						<?php $user = User::findOne($item->user_id); ?>
						<tr>
							<td><?= $event->title ?></td>
							<td><?= $user->name ?></td>
							<td><?= date('m-d-Y', strtotime($item->date)) ?></td>
							<td>$<?= $event->price ?></td>
						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>