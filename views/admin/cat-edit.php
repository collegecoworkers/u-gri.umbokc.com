<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Изменение категории';
?>

<?= $this->render('_form-cat', [
	'model' => $model,
]) ?>
