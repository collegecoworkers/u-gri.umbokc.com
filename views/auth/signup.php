<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация';
$this->params['css'] = 'style-2';
$this->params['js'] = 'main-3';
?>

<div id="wrapper-container" style="height: 70vh;">
	<div class="container object">
		<div class="work">
			<br>
			<br>
			<br>
			<br>
			<br>
			<div class="post-send">
				<div id="main-post-send"> 
					<div id="title-post-send">Регистрация</div>
					<?php $form = ActiveForm::begin(); ?>

					<?= $form->field($model, 'name')->textInput(['placeholder' => 'Имя'])->label(false) ?>
					<?= $form->field($model, 'email')->textInput(['placeholder' => 'email'])->label(false) ?>
					<?= $form->field($model, 'password')->passwordInput(['placeholder' => 'пароль'])->label(false) ?>

					<br>
					<br>
					<br>
					<div style="text-align: center;">
						<?= Html::submitButton('Зарегистрироваться', ['class' => 'btn' ]) ?>
						<br>
						<?= Html::a('Войти', ['auth/login'], ['style' => '']) ?>
					</div>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>