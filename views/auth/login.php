<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Войти';
$this->params['css'] = 'style-2';
$this->params['js'] = 'main-3';
?>

<div id="wrapper-container" style="height: 70vh;">
	<div class="container object">
		<div class="work">
			<br>
			<br>
			<br>
			<br>
			<br>
			<div class="post-send">
				<div id="main-post-send"> 
					<div id="title-post-send">Вход</div>
					<?php $form = ActiveForm::begin(); ?>

					<?= $form->field($model, 'email')->textInput(['placeholder' => 'email'])->label(false) ?>
					<?= $form->field($model, 'password')->passwordInput(['placeholder' => 'пароль'])->label(false) ?>

					<br>
					<br>
					<br>
					<div style="text-align: center;">
						<?= Html::submitButton('Войти', ['class' => 'btn' ]) ?>
						<br>
						<?= Html::a('Зарегистрироваться', ['auth/signup'], ['style' => '']) ?>
					</div>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>