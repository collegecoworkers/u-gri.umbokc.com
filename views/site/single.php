<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<aside id="fh5co-hero" class="js-fullheight">
	<div class="flexslider js-fullheight">
		<ul class="slides">
			<li style="background-image: url(<?= $event->getBigImage() ?>);">
				<div class="overlay"></div>
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 text-center js-fullheight slider-text">
							<div class="slider-text-inner">
								<h2 class="heading-title"><?= $event->title ?></h2>
								<p class="fh5co-lead"><?= $event->desc ?></p>
							</div>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
</aside>

<div id="fh5co-properties" class="fh5co-section-gray">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center heading-section animate-box">
				<h3>Описание</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<p><?= $event->content ?></p>
			</div>
		</div>
	</div>
</div>

<div id="fh5co-search-map">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="s-holder">
					<h2>Заказать билет</h2>
					<?php if($event->count <= 0): ?>
						<div class="row">
							<div class="col-xxs-12 col-xs-12 text-center">
								<p>Билетов нет</p>
							</div>
						</div>
					<?php elseif(!Yii::$app->user->isGuest): ?>
						<?php $form = ActiveForm::begin(['id' => 'contact-form','fieldConfig' => ['options' => ['tag' => false,],],]);?>
							<?= $form->field($order, 'event_id')->hiddenInput(['value'=> $event->id])->label(false); ?>
							<div class="row">
								<div class="col-md-12">
									<div class="input-field">
										<label for="from">Информация о карте:</label>
										<?= $form->field($order, 'card_num')->textInput(['class' => 'form-control', 'placeholder' => 'Номер карты'])->label(false) ?>
									</div>
								</div>
								<div class="col-md-12">
									<section>
										<div class="wide">
											<?= Html::activeTextInput($order, 'card_cvv', ['class' => 'form-control cs-select cs-select-half input-half', 'placeholder' => 'CVV карты']) ?>
											<?= Html::activeTextInput($order, 'card_date', ['class' => 'form-control cs-select cs-select-half input-half', 'placeholder' => 'Дата карты']) ?>
										</div>
									</section>
								</div>
								<div class="col-xxs-12 col-xs-12 text-center">
									<?= Html::submitButton('Оплатить', ['class' => 'btn btn-primary btn-lg' ]) ?>
								</div>
							</div>
						<?php ActiveForm::end(); ?>
					<?php else: ?>
						<div class="row">
							<div class="col-xxs-12 col-xs-12 text-center">
								<a href="/auth/login" class="btn btn-primary btn-lg">Войти</a>
							</div>
						</div>
					<?php endif ?>
				</div>
			</div>
		</div>
	</div>
</div>