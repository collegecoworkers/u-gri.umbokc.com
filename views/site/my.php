<div id="fh5co-popular-properties" class="fh5co-section-gray">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center heading-section animate-box">
				<h3>Ваши билеты</h3>
			</div>
		</div>
		<div class="row">
			<?php foreach($events as $item): ?>
				<div class="col-md-4 animate-box">
					<a href="/site/view/?id=<?= $item->id ?>" class="fh5co-property" style="background-image: url(<?= $item->getImage() ?>);">
						<span class="status">Осталось: <?= $item->count ?></span>
						<div class="prop-details">
							<span class="price">$<?= $item->price ?></span>
							<h3><?= $item->title ?></h3>
						</div>
					</a>
				</div>
			<?php endforeach ?>
		</div>
	</div>
</div>