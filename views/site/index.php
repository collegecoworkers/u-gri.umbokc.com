<?php
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\models\Category;
$categories = Category::getAll();

?>

<aside id="fh5co-hero" class="js-fullheight">
	<div class="flexslider js-fullheight">
		<ul class="slides">
			<?php foreach($new_events as $item): ?>
				<li style="background-image: url(<?= $item->getBigImage() ?>);">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-4 col-md-offset-4 col-md-pull-4 js-fullheight slider-text">
								<div class="slider-text-inner">
									<div class="desc">
										<span class="status">Новый</span>
										<h1><?= $item->title ?></h1>
										<h2 class="price">$<?= $item->price ?></h2>
										<p><a class="btn btn-primary btn-lg" href="/site/view/?id=<?= $item->id ?>">Купить</a></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</li>
			<?php endforeach ?>
		</ul>
	</div>
</aside>

<div id="fh5co-features">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center heading-section animate-box">
				<h3>Афиша</h3>
			</div>
		</div>
		<div class="row">
			<?php foreach ($categories as $item): ?>
				<div class="col-md-2 animate-box">
					<div class="feature-left">
						<div class="feature-copy">
							<h3><?= $item->title ?></h3>
							<p><a href="/site/cat/?cat=<?= $item->id ?>">Список билетов</a></p>
						</div>
					</div>
				</div>
			<?php endforeach ?>
		</div>
	</div>
</div>

<div id="fh5co-popular-properties" class="fh5co-section-gray">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center heading-section animate-box">
				<h3>Последние добавленные билеты</h3>
			</div>
		</div>
		<div class="row">
			<?php foreach($events as $item): ?>
				<div class="col-md-4 animate-box">
					<a href="/site/view/?id=<?= $item->id ?>" class="fh5co-property" style="background-image: url(<?= $item->getImage() ?>);">
						<span class="status">Осталось: <?= $item->count ?></span>
						<div class="prop-details">
							<span class="price">$<?= $item->price ?></span>
							<h3><?= $item->title ?></h3>
						</div>
					</a>
				</div>
			<?php endforeach ?>
		</div>
	</div>
</div>