  <div class="sidebar-nav">
    <ul>
      <li><a href="/account/index" class="nav-header"><i class="fa fa-fw fa-dashboard"></i> Главная</a></li>
      <li><a href="/account/user-list" class="nav-header"><i class="fa fa-fw fa-users"></i> Список пользователей</a></li>
      <li><a href="/account/cassa" class="nav-header"><i class="fa fa-fw fa-briefcase"></i>  Касса</a></li>
      <li><a href="/account/cats" class="nav-header"><i class="fa fa-fw fa-archive"></i>  Категории товаров</a></li>
      <li><a href="/account/products" class="nav-header"><i class="fa fa-fw fa-legal"></i>  Товары</a></li>
    </ul>
  </div>