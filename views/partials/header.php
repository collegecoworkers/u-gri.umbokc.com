<?php
use yii\helpers\Url;
use app\models\Category;

$categories = Category::getAll();
?>

<header id="fh5co-header-section" class="sticky-banner">
	<div class="container">
		<div class="nav-header">
			<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle dark"><i></i></a>
			<h1 id="fh5co-logo"><a href="/"><i class="icon-home"></i>Билетная<span>Касса</span></a></h1>
			<!-- START #fh5co-menu-wrap -->
			<nav id="fh5co-menu-wrap" role="navigation">
				<ul class="sf-menu" id="fh5co-primary-menu">
					<li ><a href="/">Главная</a></li>
					<li>
						<a href="#" class="fh5co-sub-ddown">Афиша</a>
						<ul class="fh5co-sub-menu">
							<?php foreach($categories as $item): ?>
								<li><a href="/site/cat/?cat=<?= $item->id ?>"><?= $item->title ?></a></li>
							<?php endforeach;?>
						</ul>
					</li>
					<?php if(Yii::$app->user->isGuest): ?>
						<li><a href="/auth/login">Войти</a></li>
					<?php else: ?>
						<?php if(Yii::$app->user->identity->is_admin == 1): ?>
							<li><a href="/admin/index">Админ панель</a></li>
						<?php endif ?>
						<li><a href="/site/my-events">Купленные билеты</a></li>
						<li><a href="/auth/logout">Выйти</a></li>
					<?php endif ?>
				</ul>
			</nav>
		</div>
	</div>
</header>
