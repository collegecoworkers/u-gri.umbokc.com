<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\models\User;
use app\assets\UserAsset;

UserAsset::register($this);
$user = User::findOne(Yii::$app->user->id);
$admin = $user->is_admin == 1;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body class=" theme-blue">

	<?php $this->beginBody() ?>
	<?= $this->render('/partials/navbar-ac', ['user' => $user]);?>

	<?php if ($admin): ?>
		<?= $this->render('/partials/sidebar-ac');?>
	<?php endif ?>

	<div class="content" <?= !$admin ? 'style="margin-left:0"': '' ?>>
		<?= $content ?>
	</div>

	<?php $this->endBody() ?>
<?php $this->registerJsFile('/ckeditor/ckeditor.js');?>
<?php $this->registerJsFile('/ckfinder/ckfinder.js');?>
<script>
	$(document).ready(function(){
		var editor = CKEDITOR.replaceAll();
		CKFinder.setupCKEditor( editor );
	})
</script>
</body>
</html>
<?php $this->endPage() ?>
